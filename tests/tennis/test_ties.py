import unittest

from collections import Counter

from tennis import ties as t


class TestTieMethods(unittest.TestCase):
    def test_assign_winners(self):
        matches = [('1', '2'), ('1', '3'), ('2', '3')]
        expected_results = ['112', '113', '132', '133', '212', '213', '232', '233']
        expected_results.sort()

        results = t.assign_winners(matches)
        results.sort()

        self.assertEqual(expected_results, results)

    def test_count_wins(self):
        match_outcomes = ['112', '113', '132']

        expected_results = [Counter({'1': 2, '2': 1}),
                            Counter({'1': 2, '3': 1}),
                            Counter({'1': 1, '2': 1, '3': 1})]

        results = t.count_wins(match_outcomes)

        self.assertEqual(expected_results, results)

    def test_count_ties(self):
        counts = [Counter({'1': 2, '2': 1, '3': 2}),
                  Counter({'1': 2, '3': 1}),
                  Counter({'1': 1, '2': 1, '3': 1})]

        expected_ties = 2

        results = t.count_ties(counts)

        self.assertEqual(expected_ties, results)

        
if __name__ == '__main__':
    unittest.main()
