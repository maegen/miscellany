#!/usr/bin/python

"""
Determine whether it is possible for two competitors to end up with the same record 
in a tournament with round-robin play where each competitor plays each other once.
"""

__author__ = "Maegen Demko"
__email__ = "mvd326@gmail.com"


import copy
import itertools
import numpy as np

from collections import Counter


def main():
    n_teams = int(input("How many teams are in the tournament? "))
    teams = ''.join([str(x) for x in range(1, n_teams+1)])
    # order doesn't matter in match play
    matches = itertools.combinations(teams, 2)
    n_matches = len(list(copy.deepcopy(matches)))
    outcomes = assign_winners(matches)
    win_counts = count_wins(outcomes)
    ties = count_ties(win_counts)

    print(f'For a tournament with {n_teams} teams, {ties} out of {2**n_matches} possible outcomes result in ties')


def assign_winners(matches):
    """Assign a winner for each match. For each match there are 2 possible
    outcomes, so there are 2^N possible win sequences, where N is the number
    of teams.

    Parameters
    ----------
    matches : itertools object
        The combinations of teams that represent matches in the
        tournament.

    Returns
    -------
    list
        A list of strings where each character in the string represents
        the winner of a match.
    """
    winners = []
    for m in matches:
        match_outcomes = []
        if winners:
            for w in winners:
                match_outcomes.append(w + m[0])
                match_outcomes.append(w + m[1])
        else:
            match_outcomes.append(m[0])
            match_outcomes.append(m[1])

        winners = match_outcomes

    return winners


def count_wins(outcomes):
    """Determine how many wins each competitor has at
    the end of round-robin play.
    
    Parameters
    ----------
    outcomes : list
        A list of strings, each of which represents the outcomes of
        the matches during the round-robin phase.

    Returns
    -------
    list
        A list of Counter objects with counts of wins by competitor for
        each of the possible match outcome sequences.

    """
    counts = []
    for each in outcomes:
        counts.append(Counter(''.join(each)))

    return counts


def count_ties(counts):
    """Determine whether it is possible that two teams are tied at
    the end of round robin play by counting the number of wins.

    Parameters
    ----------
    counts : list
        A list of Counter objects with counts of wins by competitor for
        each of the possible match outcome sequences.

    Returns
    -------
    int
        The number of match outcome sequences that result in ties.

    """
    ties = 0
    for each in counts:
        competitor_wins = list(each.values())
        max_wins = max(competitor_wins)
        n_max_wins = np.where(np.array(competitor_wins) == max_wins)
        if len(n_max_wins[0]) > 1:
            ties += 1

    return ties


if __name__ == '__main__':
    main()
