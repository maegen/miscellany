from setuptools import setup, find_packages

setup(name='miscellany',
      version='0.1',
      description='An assortment of Python scripts',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
      ],
      url='https://gitlab.com/maegen/tennis',
      author='Maegen Demko',
      author_email='mvdjse@gmail.com',
      license='MIT',
      packages=find_packages(),
      install_requires=[
          'markdown',
      ],
      include_package_data=True,
      zip_safe=False)
